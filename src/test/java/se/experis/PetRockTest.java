package se.experis;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PetRockTest {
    private PetRock rocky;

    @BeforeEach
    public void setUp() throws Exception {
        rocky = new PetRock("Rocky");

    }

    @Test
    public void getName() throws Exception{
        assertEquals("Rocky", rocky.getName());
    }

    @Test
    public void testUnhappytoStart() throws Exception {
        assertFalse(rocky.isHappy());
    }

    @Test
    public void testHappyAfterplay() throws Exception{
        rocky.playWihRock();
        assertTrue(rocky.isHappy());
    }

    @Test //(expected = IllegalStateException.class)
    @Disabled ("Exception trowing note yet defineed")
    public void nameFail() throws Exception {
        rocky.getHappyMessage();
    }

    @Test
    public void name() throws Exception {
        rocky.playWihRock();
        String msg = rocky.getHappyMessage();
        rocky.getHappyMessage();
        assertEquals("I'm happy", msg);

    }

    @Test
    public void testFavNum() throws Exception {
        assertEquals(42,rocky.getFavNumber());
    }
    @Test
    void emptyNameFail() throws Exception {
        assertThrows(IllegalArgumentException.class,
                () -> {
                    PetRock woofy = new PetRock("");
                });
    }

}
